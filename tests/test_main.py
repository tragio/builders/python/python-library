"""
Module for testing the utils.
"""
from unittest import TestCase


class MainTestCase(TestCase):
    """
    Tests for the criteria utilities.
    """

    def test_main(self):
        """
        Test the main function.
        """
        import src.__main__
        import src

        self.assertIsNone(src.main())
