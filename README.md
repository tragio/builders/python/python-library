# Python Library Builder

[![pipeline status](https://gitlab.com/tragio/builders/python/python-library/badges/master/pipeline.svg)](https://gitlab.com/tragio/builders/python/python-library/-/commits/master)

### Developing

Clone the repo and run:

```./pipenv --site-packages install --dev --skip-lock```

## Build, Test, and Push

```
./pipenv run check
./pipenv run style
./pipenv run test
./pipenv run build
./pipenv run push
```
